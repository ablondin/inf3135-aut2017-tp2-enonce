# Travail pratique 2

Dans ce deuxième travail pratique, vous devez apporter des modifications à un
programme qui permet de générer aléatoirement des labyrinthes sur la grille
carrée.

Le travail doit être réalisé **seul**. Il doit être remis au plus tard le **20
novembre 2017** à **23h59**.  À partir de minuit, une pénalité de **2% par
heure** de retard sera appliquée.

## Objectifs spécifiques

Les principaux objectifs visés sont les suivants :

- Se **familiariser** avec un logiciel développé en C par quelqu'un d'autre et
  dont la documentation n'est pas complète;
- Ajouter des **nouvelles fonctionnalités** à ce logiciel;
- **Documenter** convenablement les modifications qu'on y apporte;
- Utiliser correctement un **logiciel de contrôle de version** pour apporter
  les modifications en question.

## Description du travail

Rendez-vous sur https://gitlab.com/ablondin/inf3135-aut2017-tp2, où se trouve
le générateur de labyrinthe. Vous pouvez consulter son contenu directement dans
un navigateur et vous familiarisez avec celui-ci.

**Note** : cette étape est importante et vous devez prévoir un certain temps
pour vous familiariser avec le programme avant même d'écrire une seule ligne de
code!

Votre travail consiste à compléter les trois tâches ci-bas. Bien que ce ne soit
pas obligatoire, il est recommandé de les implémenter dans l'ordre présenté.

Notez que toutes les modifications apportées au code doivent être en *anglais*,
puisque c'est la langue utilisée dans le programme. De la même façon, votre
message de *commit* doit être écrit en anglais. Cependant, vous pouvez rédiger
votre requête d'intégration (*merge request*) en *français* ou *anglais*, selon
votre préférence, en vous assurant de bien respecter le format Markdown.

### Tâche 1 : Documentation d'une fonction

Actuellement, toutes les fonctions de chacun des modules sont documentées selon
le standard Javadoc, à l'exception de la fonction `castUnsignedInteger` du
module `parse_args`.

Votre première tâche consiste à ajouter la *docstring* à cette fonction en
respectant le style utilisé dans le reste du projet.

Même s'il s'agit d'une modification simple, qui ne contiendra sans doute qu'un
seul *commit*, vous devez créer une branche nommée exactement `task1` pour
cette tâche et créer une *merge request* vers votre branche `master` sans la
fusionner.

### Tâche 2 : Produire le graphe du labyrinthe

Un labyrinthe peut naturellement être modélisé par un [graphe non
orienté](https://fr.wikipedia.org/wiki/Th%C3%A9orie_des_graphes), dans lequel
les sommets (ou noeuds) sont les pièces, et dans lequel il existe une arête
entre deux sommets `u` et `v` (deux pièces) si et seulement s'il n'y a pas de
mur entre `u` et `v`.

Votre deuxième tâche consiste à ajouter le support d'un nouveau format de
sortie, appelé `dot`, qui est le format de fichier reconnu par
[Graphviz](http://www.graphviz.org/). Il suffit pour cela d'ajouter l'option
``--output-format dot`` à votre programme.

Voici un exemple de comportement attendu:
```sh
$ bin/tp2 --num-rows 2 --num-cols 3 --output-format dot
strict graph {
  node [shape=box];
  "(0,0)" [label="0,0", pos="0,0!"];
  "(0,1)" [label="0,1", pos="1,0!"];
  "(0,2)" [label="0,2", pos="2,0!"];
  "(1,0)" [label="1,0", pos="0,-1!"];
  "(1,1)" [label="1,1", pos="1,-1!"];
  "(1,2)" [label="1,2", pos="2,-1!"];
  "(0,2)" -- "(0,1)";
  "(1,0)" -- "(0,0)";
  "(1,1)" -- "(0,1)";
  "(1,1)" -- "(1,0)";
  "(1,2)" -- "(1,1)";
}
```
En utilisant les tubes, on peut directement générer l'image avec Graphviz:
```sh
$ bin/tp2 --num-rows 2 --num-cols 3 --output-format dot | neato -Tpng -o maze.png
```
Et on obtient l'image suivante:

![](images/maze.png)

Vous devez également colorier la solution, si elle est demandée. Voici un
exemple d'utilisation:

```sh
$ bin/tp2 --num-rows 2 --num-cols 3 --output-format dot --with-solution
strict graph {
  node [shape=box];
  "(0,0)" [label="0,0", pos="0,0!"];
  "(0,1)" [label="0,1", pos="1,0!"];
  "(0,2)" [label="0,2", pos="2,0!"];
  "(1,0)" [label="1,0", pos="0,-1!"];
  "(1,1)" [label="1,1", pos="1,-1!"];
  "(1,2)" [label="1,2", pos="2,-1!"];
  "(0,1)" -- "(0,0)";
  "(1,1)" -- "(0,1)";
  "(1,1)" -- "(1,0)";
  "(1,2)" -- "(0,2)";
  "(1,2)" -- "(1,1)";
  "(0,0)" [style=filled, fillcolor=azure, color=blue, penwidth=3];
  "(0,1)" [style=filled, fillcolor=azure, color=blue, penwidth=3];
  "(0,0)" -- "(0,1)" [penwidth=3, color=blue];
  "(1,1)" [style=filled, fillcolor=azure, color=blue, penwidth=3];
  "(0,1)" -- "(1,1)" [penwidth=3, color=blue];
  "(1,2)" [style=filled, fillcolor=azure, color=blue, penwidth=3];
  "(1,1)" -- "(1,2)" [penwidth=3, color=blue];
}
```

Et l'image résultante:

![](images/maze-solution.png)

En plus de modifier le code source, vous devrez ajouter des tests dans le
répertoire `bats` qui appuient vos modifications et mettre à jour le fichier
`README` en conséquence.

Vous devrez développer ces modifications sur une branche nommée exactement
`task2`. N'hésitez pas à séparer vos modifications en plusieurs *commits* si
nécessaire (minimalement séparer les modifications au code source des ajouts de
tests et des modifications à la documentation du fichier `README`). Comme dans
le cas de la tâche 1, vous ne devez pas fusionner votre branche sur `master`,
seulement créer une requête d'intégration.

### Tâche 3 : Préciser les paramètres à l'aide d'un fichier JSON

Finalement, votre troisième tâche consiste à ajouter un support pour lire tous
les paramètres sur l'entrée standard (`stdin`) plutôt que de les transmettre
comme options en ligne de commande. Par exemple, supposons que vous ayez dans
le répertoire courant un fichier nommé `param.json` dont le contenu est:

```js
{
    "num-rows": 8,
    "num-cols": 12,
    "start": [1, 1],
    "end": [3, 7],
    "with-solution": true,
    "walls-color": "fuchsia",
    "output-format": "png",
    "output-filename": "maze.png"
}
```

Alors votre modification devra faire en sorte que les commandes
```sh
bin/tp2 --num-rows 8 --num-cols 12 --start 1,1 --end 3,7 --with-solution --walls-color fuchsia --output-format png --output-filename maze.png
```
et
```sh
bin/tp2 < param.json
```
soient complètement équivalentes.

**Remarque**: Si l'utilisateur combine des paramètres avec une saisie sur
l'entrée standard, vous pouvez adopter le comportement que vous souhaitez. Par
exemple, dans ma solution, si quelqu'un tape
```sh
bin/tp2 --num-rows 8 < param.json
```
alors c'est équivalent à entrer la commande
```sh
bin/tp2 --num-rows 8
```
(Autrement dit, on ignore `< param.json`). En d'autres termes, le seul cas où
on lit sur l'entrée standard est quand il n'y a aucun paramètre additionnel qui
est spécifié. Par conséquent, le cas où on combine des paramètres en ligne de
commande et sur l'entrée standard ne sera donc pas considéré dans la correction
de votre travail.

On s'attend à ce que le format JSON des paramètres respecte les contraintes
suivantes:

- Les clés `"num-rows"` et `"num-cols"` sont associées à des valeurs de type
  `integer`;
- Les clés `"start"` et `"end"` sont associées à des valeurs de type `array`;
- La clé `"with-solution"` est associée à une valeur de type `boolean`;
- Les clés `"walls-color"`, `"output-format"` et `"output-filename"` sont
  associées à une valeur de type `string`.

Bien qu'il soit possible de concevoir votre propre bibliothèque pour manipuler
le format JSON, vous êtes tenus d'utiliser la bibliothèque
[Jansson](http://www.digip.org/jansson/) pour cette partie. Vous devrez en
particulier gérer les formats JSON invalides (notez que la bibliothèque Jansson
offre des fonctions qui simplifient grandement la validation):

1. Si le fichier ne respecte pas la syntaxe JSON, vous devez afficher un message
   d'erreur et retourner un code d'erreur en conséquence (vous pouvez définir
   des nouveaux codes d'erreur):
   ```
   $ cat examples/wrong-format.json
   {
       "numrows: 8
   }
   $ bin/tp2 < examples/wrong-format.json      
   Error: the input must respect the JSON format  
   ```

2. Si le fichier respecte la syntaxe JSON, mais contient des clés non valides,
   vous devez aussi afficher un message d'erreur en conséquence:
   ```
   $ cat examples/unrecognized.json
   {
       "num-rows": 8,
       "num-cols": 12,
       "maze-type": "perfect"
   }
   $ bin/tp2 < examples/unrecognized.json
   Error: unrecognized parameter "maze-type"
   ```

3. Si le type d'une valeur n'est pas le bon (`integer`, `boolean` ou `string`,
   selon le cas), alors vous devez afficher un message d'erreur. Par exemple:
   ```
   $ cat examples/wrong-type.json
   {
       "num-rows": 4,
       "num-cols": "6"
   }
   $ bin/tp2 < examples/wrong-type.json
   Error: the number of rows and columns must be an integer
   ```

4. Finalement, toutes les validations déjà faites dans le cas où on passe les
   paramètres en ligne de commande doivent être maintenues.

En plus de modifier le code source, vous devrez minimalement apporter les
modifications supplémentaires suivantes:

- Mettre à jour le `Makefile`, pour que l'édition des liens avec la
  bibliothèque Jansson soit fonctionnel;
- Mettre à jour le fichier `.gitlab-ci.yml`, pour que les tests continuent
  d'être lancés automatiquement;
- Ajouter des tests dans le répertoire `bats` qui appuient vos modifications.
- Ajouter des exemples de fichier JSON valides et invalides dans un répertoire
  `exemples` que vous pouvez utiliser pour vos tests.
- Mettre à jour le fichier `README` en conséquence.

### Dépendances entre les tâches

Il est important de bien diviser les branches et les *commits* selon les tâches
auxquelles ils se rapportent. Plus précisément, les tâches 1 et 2 sont
indépendantes, alors que la tâche 3 dépend de la nouvelle option de la tâche 2
(`--output-format dot`).

Par conséquent, il est primordial de bien structurer la "topologie" de vos
contributions pour qu'elle réflète cette indépendance et cette dépendance. Vos
branches devraient donc être organisées comme suit:

![](images/taches.png)

Ainsi, les branches `task1` et `task2` devraient démarrer du *commit* le plus
récent de la branche `master`, alors que la branche `task3` devrait démarrer du
*commit* le plus récent de la branche `task2`.

## Soumettre une contribution

Un des objectifs de ce travail pratique est de vous habituer à soumettre vos
modifications en utilisant les mécanismes offerts par le logiciel de contrôle
de versions Git et les plateformes de développement collaboratif telles que
GitLab. Plus précisément, vous devrez utiliser une *requête d'intégration* (en
anglais, *merge request*, aussi parfois appelée *pull request*). Pour cela,
vous devrez créer une branche pour chacune des trois requêtes d'intégration,
que vous devrez nommer respectivement `task1`, `task2` et `task3`. Ces noms
sont importants pour nous permettre de corriger vos travaux de façon
automatique et vous serez pénalisés si vous ne les respectez pas (c'est
extrêmement long à corriger, alors nous devons être rigides sur ce point).

Dans un premier temps, vous devez reproduire (en anglais *fork*) le programme
de base disponible pour qu'il apparaisse dans vos projets sur GitLab.

**Attention!** Assurez-vous de bien faire une copie **privée** (sinon les
autres étudiants pourront voir votre solution et vous serez alors responsable
s'il y a plagiat).

Ensuite, vous devrez donner accès à votre projet en mode **Developer** (pas
**Master**) à l'utilisateur `ablondin` (groupe 20 et 40) et `Morriar` (groupe
40). Notez que même si vous êtes dans le groupe 40 (Alexandre Terrasa), vous
devez donner accès à l'utilisateur `ablondin`, car les travaux des deux groupes
seront corrigés par Alexandre Blondin Massé.

Maintenant, supposons que vous êtes satisfait de votre première tâche, qui se
trouve sur la branche `task1`. Alors il vous suffit de pousser la branche sur
votre dépôt, puis de créer une requête d'intégration de la branche `task1` à la
branche `master` de votre dépôt. Il est recommandé de vous inspirer de la
requête d'intégration
[task0](https://gitlab.com/ablondin/inf3135-aut2017-tp2/merge_requests/1) qui a
été fusionnée au projet (je vous rappelle cependant que vous ne devez **pas**
fusionner de branche lors de la remise de votre travail).

**Attention!** En pratique, on fait une requête d'intégration d'une branche de
notre dépôt personnel vers le dépôt principal (en l'occurrence, celui de
l'utilisateur `ablondin`). Cependant, dans ce contexte académique, la requête
doit être faite à l'intérieur de votre dépôt privé, pour vous assurer que
personne d'autre ne puisse consulter votre code. Ainsi, toutes vos requêtes se
feront d'une branche nommée `task1`, `task2` ou `task3` vers la branche
`master` de votre propre dépôt.

## Tests automatiques

Actuellement, lorsqu'on entre la commande `make test`, une suite de tests est
lancée. Lorsque vous apporterez des modifications au programme, vous devez vous
assurer que celles-ci ne brisent pas les suites de tests (autrement dit, il
s'agit de **tests de régression** et vous devez en tenir compte dans vos
modifications).

Par ailleurs, vous devrez apporter des légères modifications à ces suites de
tests lorsque vous effectuerez vos modifications.

Notez qu'il est aussi possible que vous n'ayez aucune modification à apporter,
principalement lorsque vos modifications ne peuvent pas être testées
automatiquement, comme dans le cas d'une modification uniquement graphique.

## Vidéos explicatifs

Lorsqu'on apporte des modifications à un programme, il n'est pas toujours
facile de manipuler les branches et de les partager correctement. Même si vous
êtes expérimentés avec le logiciel Git, je vous recommande fortement de
visualiser au moins une fois les capsules suivantes. Il s'agit d'explications
données à la session d'automne 2016 pour le même cours, mais elles vous seront
utiles dans le cadre de ce travail également:

- [Configurer un environnement de
  travail](https://uqam.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=1fd0c248-6ade-4d27-be27-a83ad4bc7126)
  (les fichiers de configurations mentionnés dans la vidéo sont disponibles
  dans le dossier `exemples` de l'[énoncé des exercices de
  laboratoire](https://gitlab.com/ablondin/inf3135-exercices/tree/master/exemples))
- [Se synchroniser avec le dépôt
  source](https://uqam.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=a65f75af-e3c9-4376-a571-b7d8818f3c91)
- [Modifier sa requête
  d'intégration](https://uqam.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=8fcbc94e-5951-42a8-9bda-d7464ac25155)

## Contraintes

Afin d'éviter des pénalités, il est important de respecter les contraintes
suivantes:

- Votre projet doit être un clone (*fork*) **privé** du projet
  https://gitlab.com/ablondin/inf3135-aut2017-tp2. L'adjectif **privé** est
  très important si vous ne voulez pas que d'autres étudiants accèdent à votre
  solution!
- Vos trois tâches doivent se trouver sur des branches nommées **exactement**
  `task1`, `task2`, `task3`;
- **Aucune requête d'intégration** ne doit être faite vers le dépôt public
  (sans quoi les autres étudiants pourront voir vos modifications!);
- **Aucune requête d'intégration** ne doit être fusionnée sur la branche
  `master` (ou tout autre branche);
- **Aucune variable globale** (à l'exception des constantes) n'est permise;
- Votre programme doit **compiler** sans **erreur** et sans **avertissement**
  lorsqu'on entre ``make``.

Advenant le non-respect d'une ou plusieurs de ces contraintes, une pénalité de
**50%** sera appliquée systématiquement. Aucune excuse ne sera considérée.

## Remise

Votre travail doit être remis au plus tard le **20 novembre 2017** à **23h59**.
À partir de minuit, une pénalité de **2% par heure** de retard sera appliquée.

La remise se fait **obligatoirement** par l'intermédiaire de la plateforme
[GitLab](https://about.gitlab.com).  **Aucune remise par courriel ne sera
acceptée** (le travail sera considéré comme non remis).

Les travaux seront corrigés sur le serveur Java. Vous devez donc vous assurer
que votre programme fonctionne **sans modification** sur celui-ci.

## Barème

Pour chacune des trois tâches, les critères suivants seront pris en compte dans
l'évaluation :

**Qualité des "docstrings"**

- Les *docstrings* respectent le standard Javadoc;
- La documentation est bien formatée et bien aérée;
- Le format est cohérent avec les autres *docstrings* déjà présentes dans le
  projet;
- La *docstring* ne contient pas d'erreurs d'orthographe.

**Qualité de la requête d'intégration (*merge request*)**

- Le titre de la requête est significatif;
- La description de la modification apportée est claire, concise et précise. En
  particulier, elle respecte le format Markdown et l'exploite à son maximum.
- Le comportement du programme avant modification est décrit;
- Le comportement du programme après modification est décrit;
- Les messages de *commits* sont significatifs et ont un format cohérent avec
  les messages rédigés par les autres développeurs;
- La requête ne contient pas d'erreurs d'orthographe.

**Qualité du changement de code**

- Le code modifié est lisible, clair et concis;
- Le code est bien aéré, de façon cohérente avec le style de programmation déjà
  existant;
- Il utilise au maximum les fonctionnalités déjà présentes (ne pas réinventer
  la roue);
- La solution n'est pas inutilement complexe.

**Tests automatisés**

- Les modifications ne brisent pas les tests déjà présents;
- S'il y a lieu, des tests sont ajoutés pour illustrer le fait que les
  nouvelles fonctionnalités ont été correctement implémentées.

| Tâche                   | Points    |
| ----------------------- | --------- |
| Tâche 1                 |    20     |
| Tâche 2                 |    30     |
| Tâche 3                 |    50     |
| **Total**               | **100**   |
